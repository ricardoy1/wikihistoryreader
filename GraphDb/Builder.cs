﻿namespace GraphDb
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    using GraphDb.History;

    using HtmlAgilityPack;

    using Neo4jClient;

    /// <summary>
    /// The builder.
    /// </summary>
    public class Builder
    {

        private int idCount;

        private StringBuilder listOfNodesBuilder;

        private StringBuilder listOfRelBuilder;

        /// <summary>
        /// The client.
        /// </summary>
        private GraphClient client;

        /// <summary>
        /// The page.
        /// </summary>
        private IDictionary<string, HtmlDocument> page = new Dictionary<string, HtmlDocument>();

        /// <summary>
        /// The urls dictionary.
        /// </summary>
        private readonly IDictionary<string, NodeReference<Person>> personsUrl = new Dictionary<string, NodeReference<Person>>();


        /// <summary>
        /// The build.
        /// </summary>
        public void Build()
        {
            this.listOfNodesBuilder = new StringBuilder();
            this.listOfRelBuilder = new StringBuilder();
            this.client = new GraphClient(new Uri("http://localhost:7474/db/data"));
            this.client.Connect();
            this.Read("/wiki/Augustus");
            using (var writer = new StreamWriter("graph.txt"))
            {
                writer.WriteLine(this.listOfNodesBuilder.ToString());
                writer.WriteLine(this.listOfRelBuilder.ToString());
            }
        }


        /// <summary>
        /// The read.
        /// </summary>
        /// <param name="partialUrl">
        /// The partial url.
        /// </param>
        /// <returns>
        /// The <see cref="NodeReference"/>.
        /// </returns>
        public NodeReference<Person> Read(string partialUrl)
        {
            var article = new WikiArticle(partialUrl);
            var emperor = article.GetPerson();
            var name = emperor.Name;
            var node = this.client.Create(emperor);
            this.personsUrl[partialUrl] = node;
            this.AddNodeToList(node, name);

            //if (name == "Pulcheria")
            //{
            //    this.personsUrl.Add("/wiki/Pulcheria", node);
            //    var succesor = this.Read("/wiki/Marcian");
            //    this.personsUrl.Add("/wiki/Marcian", succesor);
            //    this.client.CreateRelationship(node, new Precedes(succesor));
            //    this.AddRelationShip(node, succesor);
            //}

            //if (name == "Alexios III Angelos")
            //{
            //    this.personsUrl.Add("/wiki/Alexius_III_Angelus", node);
            //    var succesor = this.Read("/wiki/Alexios_IV_Angelos");
            //    this.personsUrl.Add("/wiki/Alexios_IV_Angelos", succesor);
            //    this.client.CreateRelationship(node, new Precedes(succesor));
            //    this.AddRelationShip(node, succesor);
            //}

            if (name == "Basiliscus" || name == "Leontios")
            {
                return node;
            }

            foreach (var url in emperor.SuccessorUrls)
            {
                NodeReference<Person> succesor = null;
                if (this.personsUrl.ContainsKey(url))
                {
                    succesor = this.personsUrl[url];
                }
                if (succesor == null)
                {
                    succesor = this.Read(url);
                    // this.personsUrl[url] = succesor;
                }

                this.client.CreateRelationship(node, new Precedes(succesor));
                this.AddRelationShip(node, succesor);
            }

            return node;
        }


        private void AddNodeToList(NodeReference<Person> node, string name)
        {
            this.listOfNodesBuilder.AppendLine("(emperor" + this.GetKey(node) + ":Emperor {name:'" + name + "'}),");
        }

        private void AddRelationShip(NodeReference<Person> from, NodeReference<Person> to)
        {
            this.listOfRelBuilder.AppendLine( "(" + this.GetKey(from) + ")-[:PRECEDES]->(" + this.GetKey(to) + "),");
        }

        private string GetKey(NodeReference<Person> node)
        {
            return node.Id.ToString();
        }
    }
}
