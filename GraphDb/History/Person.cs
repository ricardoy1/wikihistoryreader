﻿namespace GraphDb.History
{
    using System.Collections.Generic;

    /// <summary>
    /// The person.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        public string DateOfBirth { get; set; }

        public string PlaceOfBirth { get; set; }

        public string DateOfDeath { get; set; }

        public string PlaceOfDeath { get; set; }

        public IEnumerable<string> SuccessorUrls { get; set; }
    }
}
