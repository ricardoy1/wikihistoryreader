﻿namespace GraphDb.History
{
    using Neo4jClient;

    /// <summary>
    /// The precedes.
    /// </summary>
    public class Precedes : Relationship, IRelationshipAllowingSourceNode<Person>,
        IRelationshipAllowingTargetNode<Person>
    {
        /// <summary>
        /// The type key.
        /// </summary>
        public static readonly string TypeKey = "PRECEDES";

        /// <summary>
        /// Initializes a new instance of the <see cref="Precedes"/> class.
        /// </summary>
        /// <param name="targetNode">
        /// The target node.
        /// </param>
        public Precedes(NodeReference targetNode)
            : base(targetNode)
        {
        }

        /// <summary>
        /// Gets the relationship type key.
        /// </summary>
        public override string RelationshipTypeKey
        {
            get { return TypeKey; }
        }
    }
}
