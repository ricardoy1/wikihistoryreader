﻿using System;

namespace GraphDb
{
    using Neo4jClient;

    //public class Person
    //{
    //    public string Name { get; set; }
    //}

    //public class HatesData
    //{
    //    public string Reason { get; set; }

    //    public HatesData()
    //    { }

    //    public HatesData(string reason)
    //    {
    //        this.Reason = reason;
    //    }
    //}

    //public class KnowsRelationship : Relationship, IRelationshipAllowingSourceNode<Person>,
    //    IRelationshipAllowingTargetNode<Person>
    //{
    //    public static readonly string TypeKey = "KNOWS";

    //    public KnowsRelationship(NodeReference targetNode)
    //        : base(targetNode)
    //    { }

    //    public override string RelationshipTypeKey
    //    {
    //        get { return TypeKey; }
    //    }
    //}

    //public class HatesRelationship : Relationship<HatesData>, IRelationshipAllowingSourceNode<Person>,
    //IRelationshipAllowingTargetNode<Person>
    //{
    //    public static readonly string TypeKey = "HATES";

    //    public HatesRelationship(NodeReference targetNode, HatesData data)
    //        : base(targetNode, data)
    //    { }

    //    public override string RelationshipTypeKey
    //    {
    //        get { return TypeKey; }
    //    }
    //}

    class Program
    {
        static void Main(string[] args)
        {
            var builder = new Builder();
            builder.Build();
            Console.Write("--END--");
            Console.ReadLine();
            //            // Create entities
            //            var refA = client.Create(new Person() { Name = "Person A" });
            //            var refB = client.Create(new Person() { Name = "Person B" });
            //            var refC = client.Create(new Person() { Name = "Person C" });
            //            var refD = client.Create(new Person() { Name = "Person D" });
            // 
            //            // Create relationships
            //            client.CreateRelationship(refA, new KnowsRelationship(refB));
            //            client.CreateRelationship(refB, new KnowsRelationship(refC));
            //            client.CreateRelationship(refB, new HatesRelationship(refD, new HatesData("Crazy guy")));
            //            client.CreateRelationship(refC, new HatesRelationship(refD, new HatesData("Don't know why...")));
            //            client.CreateRelationship(refD, new KnowsRelationship(refA));

            //            var query = new Neo4jClient.Cypher.CypherQuery(
            //                "start n=node(8) match n<-[r:HATES]-e return e.Name as Name;",
            //                new Dictionary<string, object>(),
            //                CypherResultMode.Projection);
            //            var result = client.ExecuteGetCypherResults<Person>(query);

            //var query = client
            //    .Cypher
            //    .Start(new { root = client.RootNode })
            //    .Match("person-[:HATES]->node(11)")
            //    .Return(person => person.As<Person>());

            //var persons = query.Results;
        }
    }
}
