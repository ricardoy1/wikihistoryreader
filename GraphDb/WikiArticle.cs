﻿namespace GraphDb
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;

    using Fizzler.Systems.HtmlAgilityPack;

    using GraphDb.History;

    using HtmlAgilityPack;

    public class WikiArticle
    {
        public WikiArticle(string url)
        {
            this.Url = url;
            this.LoadDocument();
        }

        public string Url { get; private set; }

        public string Title { get; private set; }

        public HtmlDocument Document { get; private set; }

        public Person GetPerson()
        {
            var infoVox = this.Document.DocumentNode.QuerySelector(".infobox");
            var person = new Person
                             {
                                 Name = this.GetName(infoVox),
                                 DateOfBirth = this.GetBornDate(infoVox),
                                 PlaceOfBirth = this.GetBornPlace(infoVox),
                                 DateOfDeath = this.GetDiedDate(infoVox),
                                 PlaceOfDeath = this.GetDiedPlace(infoVox),
                                 SuccessorUrls = this.GetSuccessors(infoVox)
                             };
            return person;
        }

        private IEnumerable<string> GetSuccessors(HtmlNode infoVox)
        {
            var succesors = infoVox.Descendants().FirstOrDefault(n => n.InnerText == "Successor");
            var children = succesors.ParentNode.QuerySelectorAll("a");
            return children.Select(child => child.Attributes["href"].Value).ToList();
        }

        private string GetName(HtmlNode infoVox)
        {
            var name = infoVox.QuerySelector(".nickname").InnerText;
            return name;
        }

        private string GetBornDate(HtmlNode infoVox)
        {
            var born = infoVox.Descendants().FirstOrDefault(n => n.InnerText == "Born");
            if (born != null)
            {
                return born.InnerText;
            }
            return string.Empty;
        }

        private string GetBornPlace(HtmlNode infoVox)
        {
            var born = infoVox.Descendants().FirstOrDefault(n => n.InnerText == "Born");
            if (born.QuerySelector("a") == null)
            {
                return string.Empty;
            }
            return born.QuerySelector("a").InnerText;
        }

        private string GetDiedDate(HtmlNode infoVox)
        {
            var died = infoVox.Descendants().FirstOrDefault(n => n.InnerText == "Died");
            if (died != null)
            {
                return died.InnerText;
            }
            return string.Empty;
        }

        private string GetDiedPlace(HtmlNode infoVox)
        {
            var died = infoVox.Descendants().FirstOrDefault(n => n.InnerText == "Died");
            if (died.QuerySelector("a") == null)
            {
                return string.Empty;
            }
            return died.QuerySelector("a").InnerText;
        }

        private void LoadDocument()
        {
            this.Document = this.GetDocument(this.Url);
            var heading = this.Document.DocumentNode.QuerySelector("#firstHeading");
            this.Title = heading.QuerySelector("span").InnerText;
        }

        private HtmlDocument GetDocument(string partialUrl)
        {
            HtmlDocument document;
            using (var webClient = new WebClient())
            {
                var html = webClient.DownloadString("http://en.wikipedia.org" + partialUrl);
                document = new HtmlDocument();
                document.LoadHtml(html);
            }
            return document;
        }
    }
}
